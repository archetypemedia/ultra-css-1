let els = document.querySelectorAll(".preview-menu");

if (els) {
	if ("content" in document.createElement("template")) {
		var template = document.querySelector("#preview-menu");
		els.forEach(el => {
			let clone = template.content.cloneNode(true);
			el.appendChild(clone);
			el.nextElementSibling.style.transition = "width 1s";
		});
	}
	document.addEventListener("click", evt => {
		let el = evt.target;
		if (el.hasAttribute("data-preview-width")) {
			let width = el.getAttribute("data-preview-width");
			let targetEl = el.closest(".preview-menu").nextElementSibling;

			if (targetEl.style.width == "") targetEl.style.width = "100%"; //Needed to trigger animation

			targetEl.style.background = "hsla(12, 100%, 40%, 0.2)";
			if (targetEl.style.width == width) {
				targetEl.style.width = "100%";
			} else {
				targetEl.style.width = width;
			}
		}
	});
}

//Duplicate items for testing...
const elsDuplicate = document.querySelectorAll("[data-duplicate]");
if (elsDuplicate) {
	elsDuplicate.forEach(el => {
		let count = el.getAttribute("data-duplicate");
		if (!count) return;
		let parent = el.parentElement;
		console.log("parent", parent);
		for (i = 1; i < count; i++) {
			let clone = el.cloneNode(true);
			parent.append(clone);
		}
	});
}

const elsPrism = document.querySelectorAll("[data-prism]");
if (elsPrism) {
	const defaultCssClasses = ["language-html", "line-numbers"];
	elsPrism.forEach(el => {
		const cssClasses = el.getAttribute("data-prism");
		const html = el.innerHTML;
		const lines = html.split("\n");
		const trimmedLines = lines.filter(line => line.trim() != "");
		const firstLine = trimmedLines[0];
		const indent = firstLine.length - firstLine.trimStart().length;
		const fixedLines = trimmedLines.map(line => {
			return line.slice(indent);
		});

		htmlFixed = fixedLines.join("\n");
		const pre = document.createElement("pre");
		const code = document.createElement("code");
		pre.append(code);
		code.textContent = htmlFixed;
		let cssClassList = cssClasses.split(" ").filter(c => c.trim() != "");
		if (!cssClassList.length) cssClassList = defaultCssClasses;
		cssClassList.map(cssClass => {
			if (cssClass.trim()) pre.classList.add(cssClass);
		});

		const parent = el.parentElement;
		parent.insertBefore(pre, el.nextSibling);
	});
}

let viewTarget = document.getElementById("view-ultra-css-1");
if (viewTarget) {
	// Load a live version of the framework
	fetch("./styles/ultra-1.css")
		.then(http_response => {
			return http_response.text();
		})
		.then(contents => {
			viewTarget.innerHTML = contents;
			Prism.highlightAll();
		})
		.catch(error => {
			console.error(error);
		});
}
